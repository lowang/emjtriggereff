#include <iostream>
#include <fstream>
#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
#include <map>
#include <assert.h>
#include <iterator>
#include <vector>
#include "algorithm"

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TSystem.h"
#include <TStyle.h>
#include "TTree.h"
#include "TTreeCache.h"
#include "TNtuple.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include <TH1.h>
#include <TEfficiency.h>

#include "Math/Vector3D.h"
#include "Math/Point3D.h"
#include "Math/Vector4D.h"

using namespace std;
using namespace ROOT::Math;


template < typename T>
std::pair<bool, int > findInVector(const std::vector<T>  & vecOfElements, const T  & element)
{
    std::pair<bool, int > result;
    auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);
    if (it != vecOfElements.end())
    {
        result.second = distance(vecOfElements.begin(), it);
        result.first = true;
    }
    else
    {
        result.first = false;
        result.second = -1;
    }
    return result;
}


int main(int argc, char *argv[])
{
  if(argc!=4){
    cerr << "Invalid arguments provided, correct format is: ./main datafile outputfile year\n";
    exit(0);
  }

  string datafile = argv[1];
  string outputfile= argv[2];
  int year = (int)atoi((argv[3]));

  vector<double> ntot(5), xsec(5);
  double Lumi;

  if(year==2016) {
    ntot = {15629253, 15210939, 11839357, 6019541, 5000};
    xsec = {6829.0, 1207.0, 120.0, 25.25, 0.02049};
    Lumi = 35.92;
  } else if(year==2017) {
    ntot = {46840955, 16882838, 11634434, 5941306, 5000};
    xsec = {6310.0, 1094.0, 99.38, 20.2, 0.02049};
    Lumi = 41.53;
  } else if(year==2018) {
    ntot = {48158738, 15466225, 10955087, 5475677, 5000};
    xsec = {6310.0, 1094.0, 99.38, 20.2, 0.02049};
    Lumi = 59.74;
  } else exit(0);

  double SFs;
  if(datafile.find("HT700to1000")!=string::npos) SFs=xsec[0]*Lumi*1000./ntot[0];
  else if(datafile.find("HT1000to1500")!=string::npos) SFs=xsec[1]*Lumi*1000./ntot[1];
  else if(datafile.find("HT1500to2000")!=string::npos) SFs=xsec[2]*Lumi*1000./ntot[2];
  else if(datafile.find("HT2000toInf")!=string::npos) SFs=xsec[3]*Lumi*1000./ntot[3];
  else SFs=1;

  TChain* ch = new TChain("TreeMaker2/PreSelection");
  cout << "input config file is: "<< datafile << endl;
  ifstream inputconfig(datafile);
  string inputfile;

  while(getline(inputconfig, inputfile))
  {
    std::cout << "input file: " << inputfile << std::endl;
    ch->Add(inputfile.c_str());
  }

  TTreeCache::SetLearnEntries(100);
  ch->SetCacheSize(200*1024*1024); //200MB cache size
  ch->SetCacheEntryRange(0, ch->GetEntries());
  ch->AddBranchToCache("*", true);
  ch->StopCacheLearningPhase();

  TH1F *ht_numer = new TH1F("HT_numer", "HT; HT (GeV); Events", 100, 500, 3500);
  TH1F *ht_denom = new TH1F("HT_denom", "HT; HT (GeV); Events", 100, 500, 3500);
  TH1F *ht_base = new TH1F("HT_base", "HT; HT (GeV); Events", 100, 500, 3500);
  //TEfficiency *ht_Eff = 0;

  TH1F *pt_numer = new TH1F("PT_numer", "Jet1 PT; PT (GeV); Events", 100, 50, 2050);
  TH1F *pt_denom = new TH1F("PT_denom", "Jet1 PT; PT (GeV); Events", 100, 50, 2050);
  TH1F *pt_base = new TH1F("PT_base", "Jet1 PT; PT (GeV); Events", 100, 50, 2050);
  //TEfficiency *pt_Eff = 0;


  TFile *f = new TFile(("./"+outputfile+".root").c_str(), "RECREATE");
 
  /////////////////////////////////
  //define ntuple variables
  /////////////////////////////////

  vector<PtEtaPhiEVector> * Jets=0;
  vector<double> * Jets_neutralEmEnergyFraction=0;
  vector<double> * Jets_chargedEmEnergyFraction=0;
  vector<XYZVector> * Tracks=0;
  vector<XYZPoint> * Tracks_referencePoint=0;
  vector<XYZPoint> * PrimaryVertices_position=0;
  vector<bool> * PrimaryVertices_isGood=0;
  vector<int> * Tracks_charge=0;
  vector<int> * Tracks_quality=0;
  vector<double> * Tracks_IP2DPV0=0;
  vector<double> * Tracks_IP2dSigPV0=0;
  vector<double> * Tracks_IP3DSigPV0=0;
  vector<double> * Tracks_dzPV0=0;
  vector<double> * Tracks_dzErrorPV0=0;
  vector<int> *TriggerPass=0;
  double MET=0;
  double HT=0;
  UInt_t RunNum;
  UInt_t LumiBlockNum;
  ULong64_t EvtNum;


  ch->SetBranchAddress("Jets", &Jets);
  ch->SetBranchAddress("Jets_neutralEmEnergyFraction", &Jets_neutralEmEnergyFraction);
  ch->SetBranchAddress("Jets_chargedEmEnergyFraction", &Jets_chargedEmEnergyFraction);
  ch->SetBranchAddress("Tracks", &Tracks);
  ch->SetBranchAddress("Tracks_referencePoint", &Tracks_referencePoint);
  ch->SetBranchAddress("PrimaryVertices_position", &PrimaryVertices_position);
  ch->SetBranchAddress("PrimaryVertices_isGood", &PrimaryVertices_isGood);
  ch->SetBranchAddress("Tracks_charge", &Tracks_charge);
  ch->SetBranchAddress("Tracks_quality", &Tracks_quality);
  ch->SetBranchAddress("Tracks_IP2DPV0", &Tracks_IP2DPV0);
  ch->SetBranchAddress("Tracks_IP2dSigPV0", &Tracks_IP2dSigPV0);
  ch->SetBranchAddress("Tracks_IP3DSigPV0", &Tracks_IP3DSigPV0);
  ch->SetBranchAddress("Tracks_dzPV0", &Tracks_dzPV0);
  ch->SetBranchAddress("Tracks_dzErrorPV0", &Tracks_dzErrorPV0);
  ch->SetBranchAddress("TriggerPass", &TriggerPass);
  ch->SetBranchAddress("MET", &MET);
  ch->SetBranchAddress("HT", &HT);
  ch->SetBranchAddress("RunNum", &RunNum);
  ch->SetBranchAddress("LumiBlockNum", &LumiBlockNum);
  ch->SetBranchAddress("EvtNum", &EvtNum);

  long nentries = ch->GetEntries();
  std::cout<< "TChain entries : "<<nentries<< std::endl;

  string triggerlist = "";
  TObjArray* ObjArray = ch->GetListOfBranches();
  for(int i=0; i<ObjArray->GetSize(); i++){
    TBranch* br = (TBranch*)ObjArray->At(i);
    string bname = br->GetName();
    if(bname.find("TriggerPass")!=std::string::npos) {
      triggerlist = br->GetTitle();
      break;
    }
  }

  //std::cout << triggerlist << std::endl;

  stringstream ss(triggerlist);
  vector<string> Trigger;
  while( ss.good() ) {
    string substr;
    getline( ss, substr, ',' );
    Trigger.push_back( substr );
  }

  pair<bool, int> resultnum;
  if(year==2016) resultnum = findInVector<string>(Trigger, "HLT_PFHT900_v");
  else if(year==2017||year==2018) resultnum = findInVector<string>(Trigger, "HLT_PFHT1050_v");
  pair<bool, int> resultden = findInVector<string>(Trigger, "HLT_IsoMu27_v");

  if(!(resultnum.first && resultden.first)) {
    std::cout << "HLT Not Found" << std::endl;
    exit(0);
  }

  for (long i=0; i<nentries; i++) { 
      if (i%10000==0||(nentries<10000&&i%1000==0)) std::cout << "  ievent=" << i <<std::endl; 
      ch->GetEntry(i);
 
      int nJets = Jets->size();
      if( !(nJets>=4 && HT>500) ) continue;
      if( !(Jets->at(0).Pt()>40 && Jets->at(1).Pt()>40 && Jets->at(2).Pt()>40 && Jets->at(3).Pt()>40) ) continue;
      if( !(Jets->at(0).Eta()<2. && Jets->at(1).Eta()<2. && Jets->at(2).Eta()<2. && Jets->at(3).Eta()<2.) ) continue;

      ht_base->Fill(HT, SFs);
      pt_base->Fill(Jets->at(0).Pt(), SFs);
      
      if(TriggerPass->at(resultden.second)==1) {
        ht_denom->Fill(HT, SFs);
        pt_denom->Fill(Jets->at(0).Pt(), SFs);
      }

      if(TriggerPass->at(resultnum.second)==1) {
        if(datafile.find("EMJ")!=string::npos) {
          ht_numer->Fill(HT, SFs);
          pt_numer->Fill(Jets->at(0).Pt(), SFs);
        } else {
          if(TriggerPass->at(resultden.second)==1) {
            ht_numer->Fill(HT, SFs);
            pt_numer->Fill(Jets->at(0).Pt(), SFs);
          }
        }
      }


  }  // loop over events

  f->cd();
  /*if(TEfficiency::CheckConsistency(*ht_pass, *ht_tot)) {
    ht_Eff = new TEfficiency(*ht_pass, *ht_tot);
    ht_Eff->Write(); 
    pt_Eff = new TEfficiency(*pt_pass, *pt_tot);
    pt_Eff->Write();
  }*/
  ht_numer->Write();
  ht_denom->Write();
  ht_base->Write();
  pt_numer->Write();
  pt_denom->Write();
  pt_base->Write();
  f->Close();

  ch->ResetBranchAddresses();
  delete Jets;
  delete Jets_neutralEmEnergyFraction;
  delete Jets_chargedEmEnergyFraction;
  delete Tracks;
  delete Tracks_referencePoint;
  delete PrimaryVertices_position;
  delete PrimaryVertices_isGood;
  delete Tracks_charge;
  delete Tracks_quality;
  delete Tracks_IP2DPV0;
  delete Tracks_IP2dSigPV0;
  delete Tracks_IP3DSigPV0;
  delete Tracks_dzPV0;
  delete Tracks_dzErrorPV0;
  delete TriggerPass;

  return 0;
}
