import sys

with open('input_2016_QCD_HT700to1000.txt', 'w') as f1:
        for i in range(127+1):
                print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8/{0}_RA2AnalysisTree.root'.format(i), file=f1)

with open('input_2016_QCD_HT1000to1500.txt', 'w') as f2:
        for i in range(55+1):
                print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8/{0}_RA2AnalysisTree.root'.format(i), file=f2)
        for i in range(105+1):
                print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8_ext1/{0}_RA2AnalysisTree.root'.format(i), file=f2)


with open('input_2016_QCD_HT1500to2000.txt', 'w') as f3:
        for i in range(44+1):
                print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8/{0}_RA2AnalysisTree.root'.format(i), file=f3)
        for i in range(90+1):
                print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8_ext1/{0}_RA2AnalysisTree.root'.format(i), file=f3)

with open('input_2016_QCD_HT2000toInf.txt', 'w') as f4:
	for i in range(21+1):
		print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8/{0}_RA2AnalysisTree.root'.format(i), file=f4)
	for i in range(35+1):
		print('root://hepxrd01.colorado.edu:1094//store/user/aperloff/ExoEMJAnalysis2020/Run2ProductionV18e/Summer16v3/QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8_ext1/{0}_RA2AnalysisTree.root'.format(i), file=f4)
