///////////////////////
//
// This macro is used to 
// plot trigger efficiency 
// for single MC or data
// processes. 
//
//

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <cstdlib>
#include <iomanip> 

#include <TROOT.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TMath.h>
#include <TGaxis.h>
#include "Math/QuantFuncMathCore.h"

using namespace std;

double PoissonErrorLow(int N){
  const double alpha = 1 - 0.6827; //1 sigma interval
  double L = (N==0) ? 0 : (ROOT::Math::gamma_quantile(alpha/2,N,1.));
  return N - L;
}
double PoissonErrorUp(int N){
  const double alpha = 1 - 0.6827; //1 sigma interval
  double U = (ROOT::Math::gamma_quantile_c(alpha/2,N+1,1.));
  return U - N;
}
double EffError(int pass, int total, bool up=true){
  int fail = total-pass;
  double eff = (double)pass/(double)total;
  double err_p = up ? PoissonErrorUp(pass) : PoissonErrorLow(pass);
  double err_f = up ? PoissonErrorUp(fail) : PoissonErrorLow(fail);
  return sqrt(pow(1-eff,2)*pow(err_p,2) + pow(eff,2)*pow(err_f,2))/(double)total;
}


int main(int argc, char *argv[])
{
  if(argc!=4){
    cerr << "Invalid arguments provided, correct format is: ./main datafile outputfile year_id\n";
    exit(0);
  }

  string input = argv[1];
  string output = argv[2];
  int year = (int)atoi((argv[3]));

  double Lumi;
  bool rebin = true;

  if(year==2016) {
    Lumi = 35.92;
  } else if(year==2017) {
    Lumi = 41.53;
  } else if(year==2018) {
    Lumi = 59.74;
  } else exit(0);

  TFile* ifile = TFile::Open((input+".root").c_str(), "READ");
  TFile* ofile = new TFile((output+".root").c_str(), "RECREATE");

  TH1F* hdenom = (TH1F*)ifile->Get("HT_base");
  TH1F* hnumer = (TH1F*)ifile->Get("HT_numer");

  vector<double> newbins;
  if(rebin){
    int sum = 0;
    newbins.push_back(hdenom->GetBinLowEdge(1));
    for(unsigned bin = 1; bin <= hdenom->GetNbinsX(); bin++){
      sum += hdenom->GetBinContent(bin);
      if(sum>10 || bin==hdenom->GetNbinsX()){
        newbins.push_back(hdenom->GetBinLowEdge(bin+1));
        sum = 0;
      }
    }
    hdenom = (TH1F*)hdenom->Rebin(newbins.size()-1, "HT_tot_rebin", newbins.data());
    hnumer = (TH1F*)hnumer->Rebin(newbins.size()-1, "HT_rebin", newbins.data());
  }

  TGraphAsymmErrors* tg = new TGraphAsymmErrors(hnumer,hdenom);

  TF1* tf = nullptr;
  TFitResult* tfr = nullptr;
  double xmin = 0;
  double xmax = hnumer->GetXaxis()->GetXmax()*0.95;
  tf = new TF1("trigErf","0.5*[0]*(1+TMath::Erf((x-abs([1]))/[2]))",xmin,xmax);
  double denom1 = 4.;
  double params[] = {0.99,xmin+(xmin-xmax)/denom1,sqrt(2.)*50.};
  tf->SetParameters(params);
  tf->SetLineColor(kRed);
  tf->SetLineWidth(2);
  gStyle->SetOptFit(0);
  gStyle->SetOptStat(0);
  TFitResultPtr tfrp = tg->Fit("trigErf","S0EM+","",xmin,xmax);
  tfr = tfrp.Get();
  ofile->cd();
  tf->SetName("fit_HT");
  tf->Write();
  tfrp->SetName("err_HT");
  tfrp->Write();

  int canvasW = 700;
  int canvasH = 550;
  int canvasWextra = 4;
  int canvasHextra = 28;
  int marginL = 95;
  int marginR = 95;
  int marginB = 75;
  int marginT = 45;
  int sizeP = 26;

  TCanvas *canv = new TCanvas("canv", "canv", canvasW+canvasWextra, canvasH+canvasHextra);
  TPad* pad = new TPad("graph","",0,0,1.0,1.0);
  double padW = pad->GetWw()*pad->GetAbsWNDC();
  double padH = pad->GetWh()*pad->GetAbsHNDC();
  pad->SetMargin(marginL/padW,marginR/padW,marginB/padH,marginT/padH);
  pad->SetTicks(1,0);
  pad->Draw();
  pad->cd();

  TLegend* leg1 = new TLegend(0.4,0.7,0.8,0.9);
  leg1->SetFillColor(0);
  leg1->SetBorderSize(0);
  leg1->SetTextFont(42);
  TGraphErrors* tgraph = NULL;

  double mu = abs(tf->GetParameter(1));
  double sigma = tf->GetParameter(2)/sqrt(2.);
  double plateau = mu+3*sigma;
  double p98 = mu+2.05*sigma;
  unsigned bplateau = hdenom->FindBin(plateau);
  double nplateau = hnumer->Integral(bplateau,-1);
  double dplateau = hdenom->Integral(bplateau,-1);
  double eupplateau = EffError(nplateau,dplateau,1);
  double ednplateau = EffError(nplateau,dplateau,0);
  double effplateau = nplateau/dplateau;
  string eps("#scale[1.3]{#font[122]{e}}");
  stringstream ss;
  ss << fixed << setprecision(0);
  ss << eps << "[^{} HT #geq " << plateau << " GeV" << "] = ";
  ss << fixed << setprecision(1);
  ss << "^{}" << effplateau*100 << "^{+" << eupplateau*100 << "}_{-" << ednplateau*100 << "} %";
  leg1->AddEntry((TObject*)NULL,ss.str().c_str(),"");
  stringstream ss2;
  ss2 << fixed << setprecision(0);
  ss2 << "98% of plateau at " << p98 << " GeV";
  leg1->AddEntry((TObject*)NULL,ss2.str().c_str(),"");

  if(tfr){
    int nbinsx = tf->GetXmax();
    tgraph = new TGraphErrors(nbinsx);
    for(int i = 0; i < nbinsx; ++i){
      tgraph->SetPoint(i,i,tf->Eval(i));
    }
    tfr->GetConfidenceIntervals(tgraph->GetN(),1,1,tgraph->GetX(),tgraph->GetEY(),0.683,false);
    tgraph->SetFillColor(kGray);
  }

  TLatex width_test_cms(0,0,"CMS");
  width_test_cms.SetTextSize(sizeP/padH);
  double posP = 1-(marginT-1)/padH;
  double uminCMS = marginL/padW;
  double umaxCMS = marginL/padW + width_test_cms.GetXsize();
  TPaveText* paveCMS = new TPaveText(uminCMS,posP,umaxCMS,1.0,"NDC");
  paveCMS->SetFillColor(0);
  paveCMS->SetBorderSize(0);
  paveCMS->SetTextFont(61);
  paveCMS->SetTextSize(sizeP/padH);
  paveCMS->AddText("CMS");

  double sizePextra = sizeP - 3;
  string prelim_text = " Preliminary";
  TLatex width_test_extra(0,0,prelim_text.c_str());
  width_test_extra.SetTextSize(sizePextra/padH);
  double uminExtra = umaxCMS;
  double umaxExtra = uminExtra + width_test_extra.GetXsize();
  TPaveText* paveExtra = new TPaveText(uminExtra,posP,umaxExtra,1.0,"NDC");
  paveExtra->SetFillColor(0);
  paveExtra->SetBorderSize(0);
  paveExtra->SetTextFont(52);
  paveExtra->SetTextSize(sizePextra/padH);
  paveExtra->AddText(prelim_text.c_str());

  double intlumi = Lumi;
  string luminormunit = "fbinv";
  int lumiprec = 1;
  stringstream fbname_;
  if(luminormunit=="fbinv") fbname_ << fixed << setprecision(lumiprec) << intlumi << " fb^{-1} (13 TeV)";
  else if(luminormunit=="pbinv") fbname_ << fixed << setprecision(lumiprec) << intlumi*1000 << " pb^{-1} (13 TeV)";
  string fbname = fbname_.str();
  TLatex width_test_lumi(0,0,fbname.c_str());
  width_test_lumi.SetTextSize(sizeP/padH);
  double umaxLumi = 1-marginR/padW;
  double uminLumi = umaxLumi - width_test_lumi.GetXsize();
  TPaveText* paveLumi = new TPaveText(uminLumi,posP,umaxLumi,1.0,"NDC");
  paveLumi->SetFillColor(0);
  paveLumi->SetBorderSize(0);
  paveLumi->SetTextFont(42);
  paveLumi->SetTextSize(sizeP/padH);
  paveLumi->AddText(fbname.c_str());

  TLine* unitline = new TLine(hnumer->GetXaxis()->GetXmin(),1,hnumer->GetXaxis()->GetXmax(),1);
  unitline->SetLineColor(kBlack);
  unitline->SetLineStyle(7);
  unitline->SetLineWidth(2);


  TH1F* hnumer2 = (TH1F*)hnumer->Clone();
  TH1F* hdenom2 = (TH1F*)hdenom->Clone();
  double scale = 0.5/hnumer2->GetMaximum();
  hdenom2->Scale(scale);
  hdenom2->GetYaxis()->SetRangeUser(0, 1.5);
  hdenom2->GetYaxis()->SetTitle("HLT PFHT efficiency");
  hdenom2->SetTitle("");
  hdenom2->SetLineColor(kBlue);
  hdenom2->SetLineStyle(2);
  hdenom2->SetFillColor(0);
  hdenom2->SetLineWidth(2);
  hdenom2->Draw("hist");

  hnumer2->Scale(scale);
  hnumer2->SetFillColor(kGray);
  hnumer2->SetLineStyle(0);
  hnumer2->Draw("hist same");

  double xmax2 = hnumer2->GetXaxis()->GetXmax();
  //TAxis* yaxis = hnumer2->GetYaxis();
  TGaxis* hyaxis = new TGaxis(xmax2, 0, xmax2, 1.5, 0, 1.5/scale, 507, "+L");
  hyaxis->SetTitleFont(42);
  hyaxis->SetLabelFont(42);
  hyaxis->SetLineColor(kBlue+2);
  hyaxis->SetTextColor(kBlue+2);
  hyaxis->SetLabelColor(kBlue+2);
  //hyaxis->SetTitleOffset(yaxis->GetTitleSize());
  //hyaxis->SetTitleSize(yaxis->GetTitleSize());
  //hyaxis->SetLabelSize(yaxis->GetLabelSize());
  //hyaxis->SetLabelOffset(yaxis->GetLabelOffset());
  //hyaxis->SetTickLength(yaxis->GetTickLength());
  hyaxis->SetTitle("number of events");
  hyaxis->Draw();

  TLegend* leg2 = new TLegend(0.6,0.45,0.8,0.55);
  leg2->SetFillColor(0);
  leg2->SetBorderSize(0);
  leg2->SetTextFont(42);
  leg2->AddEntry(hnumer2, "Numerator", "f");
  leg2->AddEntry(hdenom2, "Denominator", "l");

  if(tgraph) tgraph->Draw("3 same");
  tg->SetMarkerStyle(8);
  tg->SetMarkerSize(1);
  tg->SetMarkerColor(kBlack);
  tg->Draw("PZ same");
  tf->Draw("same");

  paveCMS->Draw("same");
  paveExtra->Draw("same");
  paveLumi->Draw("same");
  unitline->Draw("same");
  leg1->Draw("same");
  leg2->Draw("same");

  hdenom2->Draw("sameaxis");


  canv->SaveAs(("canv"+to_string(year)+".png").c_str());

  
  ofile->Close();



}
