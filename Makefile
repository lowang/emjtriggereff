CC = g++

CFLAGS = -Wall -g 
CXXFLAGS=`root-config --cflags`
LDFLAGS=`root-config --ldflags`
LDLIBS=`root-config --glibs` 

INCLUDES = -lGenVector 

LFLAGS = 

LIBS =

SRCS = $(wildcard *.cc)

OBJS = $(SRCS:.cc=.o)

MAIN = main

.PHONY: depend clean

all:    $(MAIN)
	@echo  Simple compiler named $(MAIN) has been compiled

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(CXXFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LDLIBS) $(LFLAGS) $(LIBS)

.cc.o:
	$(CC) $(CFLAGS) $(CXXFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	rm $(MAIN) $(OBJS)

depend: $(SRCS)
	makedepend $(INCLUDES) $^

